import { useContext } from "react";
import { CartContext } from "../../providers/Cart";
import formatValue from "../../utils/formatValue";
import ProductCard from "../ProductCard";
import { Container, CartContainer, CartInfo } from "./styles";

const Cart = () => {
  const { cart } = useContext(CartContext);

  return (
    <Container>
      <CartInfo>
        <h1>Meu carrinho de compras</h1>
        <label>Total - </label>
        <label>
          {formatValue(cart.reduce((acc, product) => acc + product.price, 0))}
        </label>
      </CartInfo>
      <CartContainer>
        {cart.map((product) => (
          <ProductCard key={product.name} product={product} isRemovable />
        ))}
      </CartContainer>
    </Container>
  );
};

export default Cart;
