import { Container, InputContainer } from "./styles";

interface InputProps {
  label: string;
  register: any;
  name: string;
  error: string;
  placeholder: string;
  type: string;
}

const Input = ({
  label,
  register,
  name,
  error,
  placeholder,
  type,
}: InputProps) => {
  return (
    <Container>
      <div>
        {label} {!!error && <span> - {error}</span>}
      </div>

      <InputContainer isErrored={!!error}>
        <input
          {...register(name)}
          placeholder={placeholder}
          type={type}
        ></input>
      </InputContainer>
    </Container>
  );
};

export default Input;
