import { ReactNode } from "react";
import { Container } from "./styles";

interface ButtonProps {
  onClickFunc: () => void;
  blueSchema: boolean;
  type: string;
  children: ReactNode;
}

const Button = ({
  onClickFunc,
  blueSchema = false,
  type,
  children,
}: ButtonProps) => {
  return (
    <Container type={type} blueSchema={blueSchema} onClick={onClickFunc}>
      {children}
    </Container>
  );
};

export default Button;
