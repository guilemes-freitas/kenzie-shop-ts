import { useContext } from "react";
import { ProductContext } from "../../providers/Products";
import ProductCard from "../ProductCard";
import { Container, Products, Title } from "./styles";

const ProductList = () => {
  const { products } = useContext(ProductContext);
  return (
    <Container>
      <Title>Coleção de mangás</Title>
      <Products>
        {products.map((product) => {
          return (
            <ProductCard
              isRemovable={false}
              product={product}
              key={product.id}
            ></ProductCard>
          );
        })}
      </Products>
    </Container>
  );
};

export default ProductList;
