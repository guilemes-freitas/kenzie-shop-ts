import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  width: 100%;
`;
export const Products = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 1rem;
  width: 92%;
  max-width: 1570px;
  margin-top: 2rem;
`;

export const Title = styled.h1`
  position: relative;
  margin-top: 2rem;
  margin-bottom: 2rem;
  font-size: 30px;
  color: var(--white);
  text-transform: uppercase;
  line-height: 28px;
  text-align: center;
  font-weight: 600;
`;
