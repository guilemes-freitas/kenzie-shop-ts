import { Link, useHistory } from "react-router-dom";
import Logo from "../../assets/logo.png";
import {
  Container,
  NavContainer,
  HomeLink,
  NavLink,
  ButtonContainer,
} from "./styles";

import { FiShoppingCart } from "react-icons/fi";
import { Badge } from "@material-ui/core";
import { useCart } from "../../providers/Cart";
import Button from "../Button";
import { useAuth } from "../../providers/Auth";

export default function PrimarySearchAppBar() {
  const { cart } = useCart();
  const { setAuth, token } = useAuth();
  const history = useHistory();

  const handleDisconnect = () => {
    localStorage.clear();
    setAuth(false);
    history.push("/");
  };

  return (
    <Container>
      <NavContainer>
        <Link to="/">
          <HomeLink>
            <img src={Logo} alt="logo"></img>
            <h5>Kenzie Shop</h5>
          </HomeLink>
        </Link>
      </NavContainer>
      <nav>
        <NavLink to="/cart">
          <Badge badgeContent={cart.length} color="primary">
            <FiShoppingCart size={20} />
          </Badge>
          <span> Carrinho </span>
        </NavLink>
      </nav>
      <ButtonContainer>
        {!!token ?
        <Button type="button" blueSchema={false} onClickFunc={handleDisconnect}>
        Desconectar
      </Button> :
      <>
      <Button type="button" blueSchema={false} onClickFunc={() => history.push("/login")}>
          Login
        </Button>
        <Button type="button" blueSchema={false} onClickFunc={() => history.push("/signup")}>
        Cadastro
      </Button>
      </>
        }
        
      </ButtonContainer>
    </Container>
  );
}
