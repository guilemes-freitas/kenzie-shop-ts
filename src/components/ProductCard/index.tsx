import { useContext } from "react";
import { CartContext } from "../../providers/Cart";
import Button from "../Button";
import { ContainerProduct } from "./styles";

interface Product {
  id: number;
  name: string;
  price: number;
  image: string;
  title: string;
  priceFormatted: string;
}

interface ProductProps {
  product: Product;
  isRemovable: boolean;
}

const ProductCard = ({ product, isRemovable = false }: ProductProps) => {
  const { removeFromCart, addToCart } = useContext(CartContext);
  const { name, image, priceFormatted, title } = product;

  const handleDelete = () => {
    removeFromCart(product);
  };

  const handleAdd = () => {
    addToCart(product);
  };
  return (
    <ContainerProduct>
      <img src={image} alt={name}></img>
      <h4>{title}</h4>
      <span>{priceFormatted}</span>
      {isRemovable ? (
        <Button type="button" blueSchema onClickFunc={handleDelete}>
          Remover
        </Button>
      ) : (
        <Button type="button" blueSchema={false} onClickFunc={handleAdd}>
          Comprar
        </Button>
      )}
    </ContainerProduct>
  );
};

export default ProductCard;
