import { createContext, ReactNode, useContext, useState } from "react";
import { toast } from "react-toastify";
import api from "../../services/api";

interface AuthProps {
  children: ReactNode;
}
interface dataLogin {
  username: string;
  password: string;
}

interface dataSignUp {
  username: string;
  password: string;
  email: string;
}

interface AuthProviderData {
  token: string;
  setAuth: any;
  signIn: (userData: dataLogin, history: any) => void;
  signUp: (userData: dataSignUp, history: any) => void;
}

const AuthContext = createContext<AuthProviderData>({} as AuthProviderData);

export const AuthProvider = ({ children }: AuthProps) => {
  const token = localStorage.getItem("token") || "";

  const [auth, setAuth] = useState<string>(token);

  const signIn = (userData: dataLogin, history: any) => {
    api
      .post("/sessions/", userData)
      .then((response) => {
        localStorage.setItem("token", response.data.access);
        setAuth(response.data.access);
        history.push("/");
      })
      .catch((err) => toast.error("Erro ao fazer login"));
  };

  const signUp = (userData: dataSignUp, history: any) => {
    api
      .post("/users/", userData)
      .then((_) => {
        toast.success("Sucesso ao criar a conta");
        return history.push("/login");
      })
      .catch((err) => toast.error("Erro ao criar a conta"));
  };

  return (
    <AuthContext.Provider value={{ token: auth, setAuth, signIn, signUp }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
