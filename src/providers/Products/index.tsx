import { createContext, ReactNode, useContext, useState } from "react";
import api from "../../services/api";
import formatValue from "../../utils/formatValue";

interface ProductProviderData {
  products: Product[];
  loadProducts: (setLoading: any) => void;
}

interface Product {
  id: number;
  name: string;
  price: number;
  image: string;
  title: string;
  priceFormatted: string;
}

export const ProductContext = createContext<ProductProviderData>(
  {} as ProductProviderData
);

interface ProductProps {
  children: ReactNode;
}

export const ProductProvider = ({ children }: ProductProps) => {
  const [products, setProducts] = useState([]);

  async function loadProducts(setLoading: any) {
    const response = await api.get("/products/");

    const data = response.data.map((product: Product) => ({
      ...product,
      priceFormatted: formatValue(product.price),
    }));

    setLoading(false);
    setProducts(data);
  }
  return (
    <ProductContext.Provider value={{ loadProducts, products }}>
      {children}
    </ProductContext.Provider>
  );
};

export const useProduct = () => useContext(ProductContext);
