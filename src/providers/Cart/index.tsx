import { createContext, useContext, useState } from "react";
import { ReactNode } from "react";

const initialState = JSON.parse(localStorage.getItem("@Context:Cart") || "[]");

interface CartProviderData {
  cart: Product[];
  addToCart: (product: Product) => void;
  removeFromCart: (product: Product) => void;
  finishShop: () => void;
}

export const CartContext = createContext<CartProviderData>(
  {} as CartProviderData
  //Aqui estamos tipando o objeto default do nosso context
);

interface Product {
  id: number;
  name: string;
  price: number;
  image: string;
  title: string;
  priceFormatted: string;
}

interface CartProps {
  children: ReactNode;
}

export const CartProvider = ({ children }: CartProps) => {
  const [cart, setCart] = useState(initialState);

  const addToCart = (product: Product) => {
    const list = JSON.parse(localStorage.getItem("@Context:Cart") || "[]");
    list.push(product);
    localStorage.setItem("@Context:Cart", JSON.stringify(list));
    setCart(list);
  };

  const removeFromCart = (product: Product) => {
    const list = cart.filter(
      (filtered: Product) => filtered.id !== product.id
    );
    localStorage.setItem("@Context:Cart", JSON.stringify(list));
    setCart(list);
  };

  const finishShop = () => {
    setCart([]);
    localStorage.removeItem("@Context:Cart");
  };
  return (
    <CartContext.Provider
      value={{ cart, addToCart, removeFromCart, finishShop }}
    >
      {children}
    </CartContext.Provider>
  );
};

export const useCart = () => useContext(CartContext);
