import { ReactNode } from "react";
import { CartProvider } from "./Cart";
import { AuthProvider } from "./Auth";
import { ProductProvider } from "./Products";

interface ProvidersProps {
  children: ReactNode;
}

const Providers = ({ children }: ProvidersProps) => {
  return (
    <CartProvider>
      <AuthProvider><ProductProvider>{children}</ProductProvider></AuthProvider>
    </CartProvider>
  );
};
export default Providers;
