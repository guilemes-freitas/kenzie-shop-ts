import { Link, useHistory, Redirect } from "react-router-dom";
import Button from "../../components/Button";
import Input from "../../components/Input";
import { AnimationContainer, Background, Container, Content } from "./styles";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useAuth } from "../../providers/Auth";

interface dataSignup {
  username: string;
  password: string;
  email: string;
}

function Signup() {
  const { token, signUp } = useAuth();
  const schema = yup.object().shape({
    username: yup.string().required("Campo obrigatório!"),
    email: yup.string().email("Email inválido").required("Campo obrigatório!"),
    password: yup
      .string()
      .min(5, "Mínimo de 5 digitos")
      .required("Campo obrigatório!"),
    passwordConfirm: yup
      .string()
      .oneOf([yup.ref("password")], "Senhas estão diferentes")
      .required("Campo obrigatório!"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const history = useHistory();

  const onSubmitFunction = ({ username, email, password }: dataSignup) => {
    const data = { username, email, password };
    signUp(data, history);
  };

  if (token) {
    return <Redirect to="/" />;
  }

  return (
    <Container>
      <Background />
      <Content>
        <AnimationContainer>
          <form onSubmit={handleSubmit(onSubmitFunction)}>
            <h1>Cadastro</h1>
            <Input
              type="text"
              register={register}
              name="username"
              label="Nome"
              placeholder="Seu nome de usuário"
              error={errors.username?.message}
            ></Input>
            <Input
              type="email"
              register={register}
              name="email"
              label="Email"
              placeholder="Seu melhor email"
              error={errors.email?.message}
            ></Input>
            <Input
              register={register}
              name="password"
              label="Senha"
              placeholder="Uma senha bem segura"
              type="password"
              error={errors.password?.message}
            ></Input>
            <Input
              register={register}
              name="passwordConfirm"
              label="Confirmação da senha"
              placeholder="Confirmação da senha"
              type="password"
              error={errors.passwordConfirm?.message}
            ></Input>

            <Button
              blueSchema={false}
              onClickFunc={() => console.log()}
              type="submit"
            >
              Enviar
            </Button>
            <p>
              Já tem uma conta? Faça <Link to="/login">Login</Link>
            </p>
          </form>
        </AnimationContainer>
      </Content>
    </Container>
  );
}

export default Signup;
