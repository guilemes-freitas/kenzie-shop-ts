import styled, { keyframes } from "styled-components";
import SignupImage from "../../assets/conheca-coover.png";

export const Container = styled.div`
  height: 100vh;
  max-height: 500px;
  display: flex;
  flex-wrap: wrap;
  align-items: stretch;
`;

export const Background = styled.div`
  @media (min-width: 1100px) {
    flex: 1;
    background: url(${SignupImage}) repeat, var(--darkBlue);
    background-size: cover;
    min-height: 100vh;
  }
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  max-width: 700px;
  background: var(--white);
`;

const appearFromLeft = keyframes`
from{
    opacity: 0;
    transform: translateX(50px)
}

to{
    opacity: 1;
    transform: translateX(0px)
}
`;

export const AnimationContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  animation: ${appearFromLeft} 1s;

  form {
    margin: 80px 0;
    width: 340px;
    text-align: center;
    flex-wrap: wrap;

    h1 {
      margin-bottom: 32px;
    }

    > div {
      margin-top: 16px;
    }

    p {
      margin-top: 8px;

      > a {
        font-weight: bold;
        color: var(--darkBlue);
      }
    }
  }
`;
