import Button from "../../components/Button";
import Cart from "../../components/Cart";
import { Container, ButtonContainer } from "./styles";
import { toast } from "react-toastify";
import { useContext } from "react";
import { CartContext } from "../../providers/Cart";

const CartPage = () => {
  const { cart, finishShop } = useContext(CartContext);

  const handleClean = () => {
    if (cart.length) {
      finishShop();
      toast.success("Compra finalizada!");
    } else {
      toast.error("Adicione produtos ao carrinho antes!");
    }
  };
  return (
    <Container>
      <Cart />
      <ButtonContainer>
        <Button type="button" blueSchema={false} onClickFunc={handleClean}>
          Finalizar Compra
        </Button>
      </ButtonContainer>
    </Container>
  );
};

export default CartPage;
