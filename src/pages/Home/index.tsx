import React, { useEffect, useState } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Container, ProductList } from "./styles";
import ProductCard from "../../components/ProductCard";
import { useProduct } from "../../providers/Products";


function Home() {
  const [loading, setLoading] = useState(true);
  const {loadProducts, products} = useProduct();

  useEffect(() => {
    loadProducts(setLoading);
  }, []);

  return (
    <Container>
      {loading ? (
        <CircularProgress size={50} />
      ) : (
        <ProductList>
          {products.map((product) => (
            <ProductCard product={product} isRemovable={false}></ProductCard>
          ))}
        </ProductList>
      )}
    </Container>
  );
}

export default Home;
