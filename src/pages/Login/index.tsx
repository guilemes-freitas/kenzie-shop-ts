import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Link, useHistory, Redirect } from "react-router-dom";
import { useAuth } from "../../providers/Auth";
import Button from "../../components/Button";
import Input from "../../components/Input";

import { AnimationContainer, Background, Container, Content } from "./styles";

interface dataLogin {
  username: string;
  password: string;
}

function Login() {
  const { signIn, token } = useAuth();

  const schema = yup.object().shape({
    username: yup.string().required("Campo obrigatório"),
    password: yup
      .string()
      .min(5, "Mínimo de 5 dígitos")
      .required("Campo obrigatório"),
  });

  const history = useHistory<string>();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data: dataLogin) => {
    signIn(data, history);
  };

  if (token) {
    return <Redirect to="/" />;
  }

  return (
    <Container>
      <Content>
        <AnimationContainer>
          <form onSubmit={handleSubmit(onSubmit)}>
            <h1>Login</h1>
            <Input
              type="text"
              register={register}
              name="username"
              label="Nome de usuário"
              placeholder="Seu nome de usuário"
              error={errors.username?.message}
            ></Input>
            <Input
              register={register}
              name="password"
              label="Senha"
              placeholder="Sua senha"
              type="password"
              error={errors.password?.message}
            ></Input>
            <Button
              type="submit"
              blueSchema={false}
              onClickFunc={() => console.log()}
            >
              Enviar
            </Button>
            <p>
              Não tem conta? <Link to="/signup">Faça seu cadastro</Link>
            </p>
          </form>
        </AnimationContainer>
      </Content>
      <Background />
    </Container>
  );
}

export default Login;
