import { createGlobalStyle } from "styled-components";
import SignupImage from "../assets/conheca-coover.png";

export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        outline: 0;
    }

    :root {
        --blue: #1480fb;
        --indigo: #007aff;
        --purple: #01356c;
        --white: #f9f9f9;
        --darkBlue: #05133d;
        --light-gray: #f0f0f0;
        --gray: #666360;
        --red: #c53030;
    }

    body{
        background: var(--gray);
        color: var(--darkBlue);
        background: url(${SignupImage}) repeat, var(--darkBlue);
        background-size: cover;
    }
    body,button{
        font-family: "Montserrat", sans-serif;
        font-size: 1rem;
    }

    h1,h2,h3,h4,h5,h6{
        font-family: "Montserrat", sans-serif;
        font-weight: 700;
    }

    button{
        cursor: pointer;
    }

    a{
        text-decoration: none;
    }
    
`;
