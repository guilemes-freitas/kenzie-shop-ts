import { Route, Switch } from "react-router";
import Nav from "../components/Nav";
import CartPage from "../pages/CartPage";
import Home from "../pages/Home";
import Login from "../pages/Login";
import Signup from "../pages/Signup";

function Routes() {
  return (
    <Switch>
      <Route exact path="/">
        <Nav />
        <Home />
      </Route>
      <Route exact path="/cart">
        <Nav />
        <CartPage />
      </Route>
      <Route path="/login" component={Login} />
      <Route path="/signup" exact component={Signup} />
    </Switch>
  );
}

export default Routes;
